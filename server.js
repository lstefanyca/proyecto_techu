//Variables globales
var express = require('express'); /*Variable que apunta al modulo express*/
var app =  express(); /*contiene todo lo que devuelva express*/
var bodyParser = require('body-parser');//para que se reconozca el jason enviado desde el cuerpo
app.use(bodyParser.json());//impotar librería esta instalado por defecto
var userFile = require('./user.json');//Bliblioteca para acceder archivos y ficheros creando referencia al archivo
var totalUsers = 0;//variable global contador de usuarios
const URL_BASE = '/apitechu/v1/';
//variable de entorno
const PORT = process.env.PORT || 3000;

//peticiones asincronas coollback (funcion anonima como parametro de otra funcion)
//peticion get de todos los 'users'
app.get(URL_BASE + 'users',
    /*uri , COLLBAK FUNCION ANONIMA COMO PARAMETRO DE OTRA FUNCION (request y response)*/
    function(request,response)
    {
      console.log('GET',URL_BASE+'users');
      response.send(userFile);
    }
  );
//Peticion GET de un 'user' (Instance)
//se antepone : para indicar un parámetro
app.get(URL_BASE+'users/:id',
function(request,response)
  {
  //  console.log('GET' +URL_BASE+'users/id');
  //  console.log(request.params.id);
  // let para variable local, busqueda por indice
    let indice = request.params.id;
    let instancia = userFile[indice-1];
  //  let instancia = userFile[indice];
    console.log(instancia);
  //  if (instancia !=undefined) {
  //envia respuesta
  //    response.send(instancia);
  //  }
  //    else {
  //      //response.send("NO ENCONTRADO")
  //      response.send({"mensaje":"Recurso no encontrado"});
  //    }
    let respuesta = (instancia !=undefined)? instancia : {"mensaje":"Recurso no encontrado"};
  //error http
    response.status(200);
    response.send(respuesta);

  })

//peticion GET con query string (ver en consola)
//http://localhost:3000/apitechu/v1/usersq?valor1=3&valor2="hola"
app.get(URL_BASE+'usersq',
function(request,response)
  {
    console.log('GET' +URL_BASE+' con query string');
    console.log(request.query);
    response.send(respuesta);
  })

// Petición POST enviando datos a través de BODY añadir usuario
app.post(URL_BASE + 'users',
  function(req,res){
    totalUsers = userFile.length;
    var cuerpo = Object.keys(req.body).length;
    if (cuerpo > 0) {

      let newUser = {
        userId    : totalUsers+1,
        first_name : req.body.first_name,
        last_name  : req.body.last_name,
        email     : req.body.email,
        password  : req.body.password
      };

      userFile.push(newUser);
      res.status(200);//codigo de exito al crear un nuevo recurso
      res.send({"mensaje":"Usuario creado con éxito","usuario": newUser});//para que no indique que se ha añadido conforme
    }
    else {
      res.send({"Error":"no se puede insertar vacío"});
    }

  })

// Petición PUT enviando datos a través de BODY MODIFICAR usuario
app.put(URL_BASE + 'users/:id',
   function(req,res){
     var cuerpo = Object.keys(req.body).length; //metodo para saber si el objeto recibido esta vacio
     if (cuerpo > 0) {
         let idp = req.params.id - 1;
         if(userFile[idp] != undefined){
           userFile[idp].first_name = req.body.first_name;
           userFile[idp].last_name = req.body.last_name;
           userFile[idp].email = req.body.email;
           userFile[idp].password = req.body.password;
           res.status(202);
           res.send({"mensaje" : "Usuario actualizado con exito.",
                     "Array" : userFile[idp]});
           console.log(userFile[idp]);
         }
         else{
           res.send({"mensaje":"Recurso no encontrado."})
         }
     }
     else {
       let varError = {"mensaje" : "No Existe parametros para Body"};
       res.send(varError)
       console.log(varError);
     }
   })

   // Petición DELETE enviando datos a través de BODY ELIMINAR usuario
   app.delete(URL_BASE + 'users/:id',
      function(req,res){
        let idu = req.params.id - 1;
        console.log(userFile[idu]);
        if(userFile[idu] != undefined){
          userFile.splice(idu, 1);
          res.status(204);
          res.send({"mensaje" : "Usuario eliminado con exito.",
                      "Array" : userFile[idu]});
              console.log(userFile[idu]);
            }
            else{
              res.send({"mensaje":"Recurso no encontrado."})
            }
      });

      /* OTRO EJEMPLO DE delete
    app.delete(URL_BASE + 'users', function (req, res) {
        if(reqEmpty(req)){
            console.log('Usuario a eliminar ' + JSON.stringify(req.body.userID));
            userFile.splice(req.body.userID - 1, 1);
            res.status(200);
            res.send({ "mensaje": "usuario eliminado " + JSON.stringify(userFile) });
        } else {
           res.send({ "mensaje": "No se tiene body" });
         }
    });
    */

//LOGIN
app.post(URL_BASE + 'login',
  function(req,res){
  //  console.log(req.body.email);
  //  console.log(req.body.password);
    var user = req.body.email;
    var password = req.body.password;

    for(usu of userFile){
      if(usu.email == user){
        if(usu.password == password)
        {
          usu.logged = true;
          writeUserDataToFile(userFile);
    //    console.log("Login Correcto");
          res.send ({"mensaje":"login correcto","idUsuario":usu.id,"logged":"true"});
        }
        else{
          console.log("login incorrecto");
          res.send({"mensaje":"login incorrecto"});
        }
      }

    }
  });


//LOGOUT por usuario
app.post(URL_BASE + 'logout',
  function(req,res){
  //  console.log(req.body.email);
    var user = req.body.email;
    for(usu of userFile){
      if(usu.email == user){
        var situa = String(usu.logged);
        if(situa == "true"){
          delete usu.logged;
          writeUserDataToFile(userFile);
      //    console.log("logout Correcto");
          res.send ({"mensaje":"logout correcto","idUsuario":usu.id});
        }
        else {
      //    console.log("no loggeado");
          res.send ({"mensaje":"no loggeado","idUsuario":usu.id});
        }
    }
  }
}
);

//Obtener todos los usuarios que estén logados
app.get(URL_BASE + 'loginall',
  function(req,res){
  let login = 0;
  var todos = [userFile.length];
  var y = 0;
  while(login < userFile.length){
    if(userFile[login].logged != undefined){
       todos[y] = userFile[login];
       y++;
     }
    login++;
   }
    res.send (todos);

});

//Limitar los usuarios a mostrar





//escribe en el archivo user.json
function writeUserDataToFile(data) {
   var fs = require('fs');
   var jsonUserData = JSON.stringify(data);

   fs.writeFile("./user.json", jsonUserData, "utf8",
    function(err) { //función manejadora para gestionar errores de escritura
      if(err) {
        console.log(err);
      } else {
        console.log("Datos escritos en 'users.json'.");
      }
    })
 }




//puerto 3000 es por convencion, Haciendo referencia a la variable
app.listen(PORT,function()
  {
         console.log('Api escuchando en puerto 300');
  });
//app.listen(3000);
//console.log('Api escuchando en puerto 300');
